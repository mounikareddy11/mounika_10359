package com.sample.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.util.DBUtil;

/**
 * this class provides the implementions for the methods defined in the
 * studentDAo interface
 * 
 * @author IMVIZAG
 *
 */

public class StudentDAOImpl implements StudentDAO {
	/**
	 * this method is used to insert the data into a student table
	 */
	@Override
	public boolean createStudent(Student student) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "insert into student_tbl values( ?, ?, ?,?,?,?)";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, student.getId());
			ps.setString(2, student.getName());
			ps.setInt(3, student.getAge());
			ps.setString(4,student.getAddress().getCity());
			ps.setString(5,student.getAddress().getState());
			ps.setString(6,student.getAddress().getPincode());
			Address address =new Address();
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				result = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/***
	 * this method is used to search a student by using student id. returns student
	 * object if id found
	 */

	@Override
	public Student searchById(int id) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Student st = null;
		List<Student> studentList = new ArrayList<Student>();
		String sql = "select * from student_tbl where id = ?";
		
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				Address address=new Address();
				address.setCity(rs.getString(4));
				address.setState(rs.getString(5));
				address.setPincode(rs.getString(6));
				st = new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				st.setAddress(address);
               studentList.add(st);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return st;
	}

	/***
	 * this method is used to display all the student details.
	 */
	@Override
	public List<Student> getAllStudents() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Student st = null;
		List<Student> studentList = new ArrayList<Student>();
		String sql = "select * from student_tbl ";
		try {
			con = DBUtil.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				Address address=new Address();
				address.setCity(rs.getString(4));
				address.setState(rs.getString(5));
				address.setPincode(rs.getString(6));
				st = new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				st.setAddress(address);
				studentList.add(st);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return studentList;
	}

	/***
	 * this deleteStudent method is used to delete a particular student from the
	 * student table.
	 */
	@Override
	public boolean deleteStudent(int id) {

		Connection con = null;
		PreparedStatement ps = null;

		String sql = "delete from student_tbl where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);

			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				return true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return false;
	}

	/**
	 * this updateStudent method is used to update a particular student details by
	 * usiung student id .
	 */
	public boolean updateStudent(Student student) {

		Connection con = null;
		PreparedStatement ps = null;

		String sql = "update student_tbl set name = ?,age = ? where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, student.getName());
			ps.setInt(2, student.getAge());
			ps.setInt(3, student.getId());

			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				return true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return false;
	}

}
