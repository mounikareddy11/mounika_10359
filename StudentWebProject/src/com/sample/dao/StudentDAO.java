package com.sample.dao;

import java.util.List;

import com.sample.bean.Student;

/***
 * this interface cvvontains the abstract methods
 * 
 * @author IMVIZAG
 *
 */
public interface StudentDAO {
	/**
	 * this metod is used to insert record into a student tasble
	 * 
	 * @param student
	 * @return
	 */
	boolean createStudent(Student student);

	/***
	 * this method is used to search a student by using id
	 * 
	 * @param id
	 * @return true if id found
	 */
	Student searchById(int id);

	/***
	 * this method is used to display all the student details
	 * 
	 * @return
	 */
	List<Student> getAllStudents();

	/***
	 * this method is used to update a student data by using student details
	 * 
	 * @param id
	 * @returns true if student deletd successfully
	 */
	boolean deleteStudent(int id);

	/***
	 * this method is used to delete a student data by using student object
	 * 
	 * @returns true if student deleted successfully
	 */

	boolean updateStudent(Student student);
}
