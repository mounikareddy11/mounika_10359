package com.sample.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
/***
 * this class is used for getting the connection from the server
 * @author IMVIZAG
 *
 */
public class DBUtil {
   /***
    * thiis method is used to register driver and getting connection from the server
    * @returns connection object if the connection is established
    */
	public static Connection getCon() {
		Connection con = null;
		
		try {
			//1.loading the driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.println("driver is loaded");
			
			//2.establish the connection
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/student", "root", "innominds");
			System.out.println("connected to database");
			
		
			
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return con;
	}
	
	
	
}
