package com.sample.service;

import java.util.List;

import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;

public class StudentServiceImpl implements StudentService {
	/***
	 * this method is used to call the createStudent method which is used to insert
	 * the data into a student table. it returns the true if student registered
	 * successfully
	 */
	@Override
	public boolean insertStudent(Student student) {
		// TODO Auto-generated method stub

		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.createStudent(student);

		return result;
	}

	/***
	 * this method is used to call the searchById method presented in studentDAO it
	 * returns the id if the id is found
	 */
	@Override
	public Student findById(int id) {

		StudentDAO studentDAO = new StudentDAOImpl();
		Student student = studentDAO.searchById(id);
		return student;
	}

	@Override
	/***
	 * this method is used to call the getAllStudents method presented in studentDAO
	 */
	public List<Student> fetchAllStudents() {

		StudentDAO studentDAO = new StudentDAOImpl();
		List<Student> studentList = studentDAO.getAllStudents();
		return studentList;
	}

	/***
	 * this method is used to call the deleteStudent method presented in studentDAO
	 * which is used to delete the data from student table
	 */
	public boolean deleteStudent(int id) {

		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.deleteStudent(id);

		return result;
	}

	/***
	 * this method is used to call the updateStudent method presented in studentDAO
	 * which is used to upadate the data in student table
	 */
	public boolean updateStudent(Student student) {

		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.updateStudent(student);

		return result;

	}

}
