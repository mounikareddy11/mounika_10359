package com.sample.bean;
/***
 * this class is used to generate setters and getters for address
 * @author IMVIZAG
 *
 */

public class Address {
	private String city;
	private String state;
	private String pincode;
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	

}
