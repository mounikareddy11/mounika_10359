package com.sample.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;

/**
 * Servlet implementation class SearchById
 */
@WebServlet("/SearchById")
public class SearchById extends HttpServlet {
	private static final long serialVersionUID = 1L;
<<<<<<< HEAD

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SearchById() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int id = Integer.parseInt(request.getParameter("id"));
		Student student = new Student();
		StudentDAO service = new StudentDAOImpl();
		student.setId(id);
		Student st = service.searchById(id);
		RequestDispatcher rd = request.getRequestDispatcher("Search.jsp");
		if (st != null) {
			request.setAttribute("student", st);
			rd.forward(request, response);
		} else {
			response.sendRedirect("error.html");

		}
=======
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchById() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    int id = Integer.parseInt(request.getParameter("id"));
	    Student student = new Student();
	    StudentDAO service=new  StudentDAOImpl();
	    student.setId(id);
	   Student st =  service.searchById(id);
	    RequestDispatcher rd=request.getRequestDispatcher("Search.jsp");
	    if(st!=null) {
	    	request.setAttribute("student",st);
	    	 rd.forward(request,response);
	    }else {
	    	response.sendRedirect("error.html");
	    	
	    }
>>>>>>> 4412e924f9e808bada8872539f622acd504dd525
	}

}
