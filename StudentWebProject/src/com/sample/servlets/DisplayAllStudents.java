package com.sample.servlets;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudents")

public class DisplayAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   	// object creation for studentserviceImpl
   		StudentService service = new StudentServiceImpl();
   		//calling fetchAllStudents method 
   		List<Student> studentList = service.fetchAllStudents();
   		request.setAttribute("studentList", studentList);
   		//creating object for requestDispatcher
   		RequestDispatcher rd=request.getRequestDispatcher("Display_all.jsp");
   		//forwarding request to Display.jsp
   		rd.forward(request, response);
   		
		}

}
