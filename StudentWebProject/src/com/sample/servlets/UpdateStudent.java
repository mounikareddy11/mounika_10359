package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class UpdateStudent
 */
@WebServlet("/UpdateStudent")
public class UpdateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//converting the id string to integer
		int studentId = Integer.parseInt(request.getParameter("Id"));
		String studentName = request.getParameter("name");
		String studentAge = request.getParameter("age");
		
		boolean result = false;
		
		StudentService service = new StudentServiceImpl();
		PrintWriter out = response.getWriter();
		//calling the findById method by using service object
		Student student = service.findById(studentId);
		Student studentUpdate = new Student();
		if(student != null) {
			if(studentName.equals("") || studentName.equals(" ") ) {
				studentUpdate.setId(student.getId());
				studentUpdate.setName(student.getName());
				studentUpdate.setAge(Integer.parseInt(studentAge));
				
			}
			else if(studentAge.equals("") || studentAge.equals(" ") ) {
				studentUpdate.setId(student.getId());
				studentUpdate.setName(studentName);
				studentUpdate.setAge(student.getAge());
			}
			else {
				studentUpdate.setId(student.getId());
				studentUpdate.setName(studentName);
				studentUpdate.setAge(Integer.parseInt(studentAge));
				
			}
		
		   // calling the updateStudent method
			result = service.updateStudent(studentUpdate);
	   		
			out.print("<html><body>");
			
			if(result) {
				out.print("<h2>Student Details updated</h2>");
			}
			else {
				out.print("<h2>Something wrong</h2>");
			}

		}else {
			 out.print("<h3>Student ID not found</h3>");
		}
		
		 
		out.print("</body></html>");
		// closing the printwriter
		out.close();
	}
	

	}

