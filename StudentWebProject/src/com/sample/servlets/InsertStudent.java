package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.servlet4preview.RequestDispatcher;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/InsertStudent")
public class InsertStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//converting the id string to integer
	  List<Student> studentList=new ArrayList<Student>();
		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentName = request.getParameter("name");
		int studentAge = Integer.parseInt(request.getParameter("age"));
		String city=request.getParameter("city");
		String state=request.getParameter("state");
		String pincode=request.getParameter("pincode");
		//creating object for student	
		Student student = new Student();
		//setting the values for student
		student.setId(studentId);
		student.setName(studentName);
        student.setAge(studentAge);
        
		Address address=new Address();
		address.setCity(city);
		address.setState(state);
		address.setPincode(pincode);
		student.setAddress(address);
		studentList.add(student);
		
		
		StudentService service = new StudentServiceImpl();
		// calling the insertstudent method by using StudentService object
		boolean result = service.insertStudent(student);
				
		if(result) {
			StudentDAO std=new StudentDAOImpl();
			
			request.setAttribute("studentList", std);
			javax.servlet.RequestDispatcher rd=request.getRequestDispatcher("insert.jsp");
					rd.forward(request, response);
		}else {
			response.sendRedirect("error.html");
		}
		
	}

}
