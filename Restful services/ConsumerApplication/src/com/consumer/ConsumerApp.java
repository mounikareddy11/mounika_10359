package com.consumer;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * This class is a consumer class which is meant to make a request to the web
 * resource.
 * 
 * @author IMVIZAG
 *
 */
public class ConsumerApp {

	public static void main(String[] args) {
		// web resource url
		String url = "http://localhost:2019/JerseyFirstApp/rest/message/get";
		// creating empty client object
		Client c = Client.create();
		// Adding url to client object
		WebResource wr = c.resource(url);
		// sending request to web resource
		ClientResponse cr = wr.get(ClientResponse.class);
		// Retriving entity from response object
		String res = cr.getEntity(String.class);
		// printing response
		System.out.println(res);
		System.out.println(cr.getStatus());
	}

}
