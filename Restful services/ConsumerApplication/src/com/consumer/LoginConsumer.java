package com.consumer;

import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * This class contains consumer code which is responsible for using web services
 * 
 * @author IMVIZAG
 *
 */
public class LoginConsumer {
	/**
	 * This method makes a Login request to the web service identified by respective
	 * url
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public String doLoginRequest(String username, String password) {
		// preparing url
		String url = "http://localhost:2019/LoginProvider/rest/login";
		// creating empty client object
		Client client = Client.create();
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		// Adding url to the client object which returns web resource object
		WebResource resource = client.resource(url);
		// Adding query params to the url
		queryParams.add("username", username);
		queryParams.add("password", password);

		String uname = resource.queryParams(queryParams).post(String.class);
		return uname;
	} // End of doLoginRequest

}
