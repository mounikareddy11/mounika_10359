package com.login.consumer;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * This class contains consumer code which is responsible for using web services
 * 
 * @author IMVIZAG
 *
 */
public class LoginConsumer {
	/**
	 * This method makes a Login request to the web service identified by respective
	 * url
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public String doLoginRequest(String username, String password) {
		// preparing url
		String url = "http://localhost:2019/LoginProvider/rest/login";
		// creating empty client object
		Client client = Client.create();
		// passing url to the client object which returns web resource object
		WebResource resource = client.resource(url);
		// passing parameters
		resource = resource.queryParam("username", username);
		resource = resource.queryParam("password", password);
		// making request to web service
		ClientResponse res = resource.accept(MediaType.TEXT_PLAIN).post(ClientResponse.class);
		// getting entity from response
		String str = res.getEntity(String.class);
		return str;
	} // End of doLoginRequest

}
