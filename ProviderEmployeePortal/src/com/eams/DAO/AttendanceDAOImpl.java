package com.eams.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.eams.bean.Employee;
import com.eams.bean.Leave;
import com.eams.util.DBConnectionUtil;
/**
 * This is the AttendanceDAOImpl class where various Attendance fetching methods are implemented.
 * @author Batch - G
 *
 */
public class AttendanceDAOImpl implements AttendanceDAO {
	/**
	 * this method is used to insert attendance values into a database.
	 */
	@Override
	public boolean Attendance(int id) {
		Connection con = null;
		PreparedStatement ps = null;
		Calendar cal = null;
		cal = Calendar.getInstance();

		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		@SuppressWarnings("unused")
		String formatted = format1.format(cal.getTime());
		String sql = " insert into attendance_data values (?, ?) ";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			
			ps.setInt(1, id);
			ps.setString(2, formatted );
			int rowsEffected = ps.executeUpdate();
			System.out.println(rowsEffected);
			
		
		}
		catch(SQLException e) {	
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
		
		}
	/**
	 * This method is used displaying the employee attendance details to an Admin.
	 */
	@SuppressWarnings("unused")
	@Override
	public List<Employee> AttendanceDetails() {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		con = DBConnectionUtil.getCon();
		List<Employee> empl = new ArrayList<Employee>();
		Employee employee2 = null;
		String sql = "select emp_id,count(Distinct Date) as present_days from attendance_data group by emp_id";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
//			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
			employee2 = new Employee();
			employee2.setEmp_id(rs.getInt(1));
			employee2.setPresent_days(rs.getInt(2));
			
			empl.add(employee2);
		}
		}
		catch(SQLException e) {	
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return empl;
		}
	/**
	 *  This method is used displaying the employee attendance details to an Employee.
	 */
	@SuppressWarnings("unused")
	@Override
	public List<Employee> fetchAttendance(int id) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		con = DBConnectionUtil.getCon();
		List<Employee> empl = new ArrayList<Employee>();
		Employee employee2 = null;
		String sql = "select emp_id,count(Distinct Date) as present_days from attendance_data where emp_id = ?";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
			employee2 = new Employee();
			employee2.setEmp_id(rs.getInt(1));
			employee2.setPresent_days(rs.getInt(2));
			
			empl.add(employee2);
		}
		}
		catch(SQLException e) {	
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return empl;
		
	}
	/**
	 *  This method is used to apply for a casual leave by an employee
	 */
	@SuppressWarnings("unused")
	@Override
	public boolean ApplyCausalleave(Leave leave) {
		Connection con = null;
		PreparedStatement ps = null;
		String sql = " insert into causal_leaves values (?, ?, ?)  ";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			
			ps.setInt(1, leave.getEmp_id());
			ps.setString(2, leave.getDate_of_leave());
			ps.setString(3, leave.getReason());
			int rowsEffected = ps.executeUpdate();

		}
		catch(SQLException e) {	
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
		
		}
	/**
	 * This method is used to apply for a Sick leave by an employee
	 */
	@SuppressWarnings("unused")
	@Override
	public boolean ApplySickleave(Leave leave) {
		Connection con = null;
		PreparedStatement ps = null;
		String sql = " insert into sick_leaves values (?, ?, ?)  ";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			
			ps.setInt(1, leave.getEmp_id());
			ps.setString(2, leave.getDate_of_leave());
			ps.setString(3, leave.getReason());
			int rowsEffected = ps.executeUpdate();

			
		
		}
		catch(SQLException e) {	
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
		
		}
	}


