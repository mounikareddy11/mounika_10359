package com.eams.services;


import com.eams.bean.Admin;


/**
 * This is the AuthenticationServices interface where various validation is done.
 * @author Batch - G
 *
 */
public interface AdminServices {
	/**
	 * this method is used to insert a new admin into database;
	 * @param adminDetails
	 * @return
	 */
	boolean insertAdmin( Admin adminDetails );
	/**
	 *  this method is used to search an admin from the database.
	 * @param username
	 * @return
	 */
	Admin findByUsername(String username);
	/**
	 * this method is used to delete an admin from the database.
	 * @param username
	 * @return
	 */
	boolean deleteAdmin( String username );
	/**
	 * this method is used to login with his username and password
	 * @param username
	 * @param password
	 * @return
	 */
	boolean AdminLogin(String username,String password);
	
}
 