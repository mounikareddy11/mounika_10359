package com.abc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SampleController {
	@GetMapping("sample")
	public String sayHello(ModelMap map) {
		String modelData="welcome to spring mvc";
		map.addAttribute("data",modelData);
		return "display";
		
	}

}
