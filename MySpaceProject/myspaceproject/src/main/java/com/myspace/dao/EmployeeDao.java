package com.myspace.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.myspace.entities.Employee;

@Repository
public interface EmployeeDao extends CrudRepository<Employee, Integer> {

}
