package com.eams.controller;

import java.util.List;
import java.util.Scanner;
import com.eams.bean.Admin;
import com.eams.bean.Employee;
import com.eams.service.AdminServieImp;
import com.eams.service.EmployeeServiceImpl;


/**
 * This is the main class where the users (admin & employee) can select their respective login pages.
 * @author Batch-G
 *
 */
public class Home {
	/**
	 * This is the main method where the users (admin & employee) can select their respective login pages.
	 */
	static com.eams.service.AdminServices authentication = new AdminServieImp();
	static Employee employee = new Employee();	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Welcome to Innominds");
		System.out.println("");
		// This method has portals for different users
		login();
	}
	/**
	 * Using this method where the users (admin & employee) can select their respective login pages.
	 */
	public static void login() {
		// TODO Auto-generated method stub
		int choice = 0;
		boolean flag = false;
		while(!flag) {
			try {
				System.out.println("============================");
				System.out.println("Select User");
				System.out.println("============================");
				AdminServieImp authenication=new AdminServieImp();
				System.out.println("1) Admin");
				System.out.println("2) Employee");
				System.out.println("-----------------------------");
				Scanner s=new Scanner(System.in);
				choice=s.nextInt();
				if(choice == 1 || choice==2)
				flag = true;
			} catch(Exception e){
				System.out.println("Please enter valid input");
				flag = false;
			}
		}
		
		switch(choice){
		case 1:
			//This method has two portals where Admin and Employee can access their accounts.
			AdminLogin();
			break;
		case 2:
			EmployeeLogin();
			break;
		}
	}
	/**
	 * Using this method  the Admin can login into his account.
	 */
	public static void AdminLogin() {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter username");
		String username=scan.next();
		System.out.println("Enter password");
		String password=scan.next();
		Admin.setAdmin_username(username);
		Admin.setAdmin_password(password);
		Admin adminDetails=authentication.findByUsername(username);
	
		if(adminDetails!=null) {
			System.out.println("hello");
			if(username.equals(adminDetails.getAdmin_username()) && password.equals(adminDetails.getAdmin_password())) {
				System.out.println("Login Successfull");
				AdminModule.operations();
			}
		} else {
			System.out.println("Try Again!!!");
			AdminLogin();
			return;
		}		
	}
	/**
	 * This is the Employeelogin method where the Employees can login into their respctive accounts.
	 */
	private static void EmployeeLogin() {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter username");
		String username=scan.next();
		System.out.println("Enter password");
		String password=scan.next();
		Employee user = new Employee();
		Employee.setEmp_name(username);
		Employee.setPassword(password);
		EmployeeServiceImpl employeeImp=new EmployeeServiceImpl();
		//EmployeeDAOImpl user= new EmployeeDAOImpl();
		boolean user1 = employeeImp.employeeLogin(user);
		if (user1) {
			System.out.println("Login Successful..!");
			EmployeeOperations op= new EmployeeOperations();
			op.EmployeeOpertaions(username);
		}
		else {
			System.out.println("please enter valid login credentials...!");
			EmployeeLogin();
		}
		}
		
		
	}
	      
	


