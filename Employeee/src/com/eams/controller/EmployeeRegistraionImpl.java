package com.eams.controller;

import java.util.Scanner;

import com.eams.bean.Employee;
import com.eams.service.EmployeeServiceImpl;
import com.eams.validations.Validation;



public class EmployeeRegistraionImpl {
	int flag=0;
	Scanner sc = new Scanner(System.in);
	/**
	 * This employeeRegstrationImpl method is used to  Register the new employee by using valid employee details.
	 */
	public void employeeRegstrationImpl() {   
		Employee employee = new Employee();
		String name="";
		String password ="";
		String mobileNumber="";
		String emailId="";
		while (flag == 0) {
			System.out.println("enter emp_id");
			int emp_id = 0;
			try {
				emp_id = sc.nextInt();
				while (emp_id < 0) {
					System.err.println("please enter the valid choice");
					emp_id = sc.nextInt();
				}
			} catch (Exception e) {

			}
			employee.setEmp_id(emp_id);
			flag = 1;
		} 
		boolean check=false;
		while(!check) {
		System.out.println("enter name");
		 name = sc.next();
		if(Validation.checkName(name)) {
			break;
		}else {
			System.out.println("Enter a valid userName");
		}
		}
		flag = 0;
		while (flag == 0) {
			employee.setEmp_name(name);
			if (employee.getEmp_name() != null) {
				flag = 1;
			}
		}

		flag = 0;
		while (flag == 0) {
			while(!check) {
			System.out.println("enter password");
			password = sc.next();
			if(Validation.passwordCheck(password)) {
				break;
			}else {
				System.out.println("password should contain atleast one capital letter ,one special character and one number");
			}
			}
			employee.setPassword(password);
			if (employee.getPassword() != null) {
				flag = 1;
			}
		}
		flag = 0;
		while (flag == 0) {
			System.out.println("enter confirm password");
			String confirmPassword = sc.next();
			employee.setConfirm_password(confirmPassword);
			if (employee.getConfirm_password() != null) {
				flag = 1;
			}
		}
		flag = 0;
		while (flag == 0) {
			while(!check) {
			System.out.println("enter mobile number");
			 mobileNumber = sc.next();
			if(Validation.mobileCheck(mobileNumber)) {
				break;
			}else {
				System.out.println("mobile number contains only 10 numbers");
			}
			}
			employee.setPhone_number(mobileNumber);
			if (employee.getPhone_number() != null) {
				flag = 1;
			}
		}
		System.out.println("enter address");
		String address = sc.next();
		employee.setAddress(address);
		if (employee.getAddress() != null) {
			flag = 1;
		}
		flag = 0;
		while (flag == 0) {
			while(!check) {
			System.out.println("enter emailId");
			 emailId = sc.next();
			 if(Validation.checkEmailId(emailId)) {
				 break;
			 }else {
				 System.out.println("enter correct emailid");
			 }
			}
			employee.setEmail(emailId);
			if (employee.getEmail() != null) {
				flag = 1;
			}
		}
		flag = 0;
		while (flag == 0) {
			System.out.println("Enter date of joining (yyyy-mm-dd)");
			String date_of_joining = sc.next();
			employee.setDate_of_joining(date_of_joining);
			if (employee.getDate_of_joining() != null) {
				flag = 1;
			}
		}
		flag = 0;
		while (flag == 0) {
			System.out.println("enter Department_Id: ");
			int dept_id = 0;
			try {
				dept_id = sc.nextInt();
				while (dept_id < 0) {
					System.err.println("please enter the valid choice");
					dept_id = sc.nextInt();
				}
			} catch (Exception e) {

			}
			employee.setDepartment_id(dept_id);
			flag = 1;
		}

		System.out.println("Registered successful....");
		new EmployeeServiceImpl().insertEmployee(employee);
		System.out.println("=============================");
		AdminModule.operations();

	}
}
