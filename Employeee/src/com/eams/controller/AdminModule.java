package com.eams.controller;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import com.eams.bean.Employee;
import com.eams.dao.EmployeeDAOImpl;
import com.eams.service.EmployeeServiceImpl;

public class AdminModule {
	
	static Home home=new Home();

	public static void operations() {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int Choice=0;
		boolean flag = false;
		while(!flag) {
			try {
				System.out.println("=============================");
				System.out.println("choose");
				System.out.println("1.New Employee Registration");
				System.out.println("2.Update Employee Details");
				System.out.println("3.Delete Employee Details");
				System.out.println("4.Display Employee Details");
				System.out.println("5.Display particular Employee Details");
				System.out.println("6.Logout");
				System.out.println("=============================");
				@SuppressWarnings("resource")
				Scanner s=new Scanner(System.in);
				Choice = s.nextInt();
				if (Choice == 1 || Choice == 2 || Choice==3 || Choice==4 || Choice==5) 
					flag = true;
			}catch(Exception e) {
				System.out.println("Please enter valid input");
				System.out.println("===================================");
				flag = false;
			}
		}
		switch(Choice) {
		case 1:
			EmployeeRegistraionImpl user = new EmployeeRegistraionImpl();
			user.employeeRegstrationImpl();
			
			break;
		case 2:
			System.out.println("Enter Employee Id:");
			int update = sc.nextInt();
			System.out.println("Enter New Phone number:");
			String updateNo = sc.next();
			Employee update1 = new Employee();
			update1.setPhone_number(updateNo);
			update1.setEmp_id(update);
			EmployeeServiceImpl updateData = new EmployeeServiceImpl();
			updateData.updateEmployee(update, updateNo);
//			List <Employee> updateList = new EmployeeDAOImpl().updateEmployeeDAO(update, updateNo);
			System.out.println("phone number updated" );
			AdminModule.operations();
			break;
		case 3:
			System.out.println("Enter Employee Id:");
			int delete = sc.nextInt();
			EmployeeServiceImpl deleteData = new EmployeeServiceImpl();
			deleteData.deleteEmployee(delete);
			AdminModule.operations();
			break;
		case 4:
			EmployeeServiceImpl userData = new EmployeeServiceImpl();
			userData.fetchAllEmployee();
			List <Employee> empList = new EmployeeDAOImpl().fetchAllEmployeeDAO();
			Iterator itr = empList.iterator();
			
				System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
		        System.out.printf("%20s %20s %20s %20s %20s %20s ", "Emp_id","Name", "Phone_Number", "Email", "Address","Date_of_Joining");
		        System.out.println();
				System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");

				while(itr.hasNext()) {
					Employee emp = (Employee) itr.next();
		            System.out.format("%20s %20s %20s %20s %20s %20s" ,emp.getEmp_id(),emp.getEmp_name(),emp.getPhone_number(),emp.getEmail(),emp.getAddress(),emp.getDate_of_joining());
		            System.out.println();
		        
					System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
			}
			AdminModule.operations();
			break;
		case 5:			
			System.out.println("Enter Employee Id:");
			int user1 = sc.nextInt();
			List <Employee> list = new EmployeeDAOImpl().findByIdDAO(user1);
			Iterator<Employee> itre = list.iterator();
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
	        System.out.printf("%20s %20s %20s %20s %20s %20s ", "Emp_id","Name", "Phone_Number", "Email", "Address","Date_of_Joining");
	        System.out.println();
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");

			while(itre.hasNext()) {
				Employee emp = itre.next();
	            System.out.format("%20s %20s %20s %20s %20s %20s" ,emp.getEmp_id(),emp.getEmp_name(),emp.getPhone_number(),emp.getEmail(),emp.getAddress(),emp.getDate_of_joining());
	            System.out.println();
	        
				System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------");
		}	
			break;
		case 6:
			System.out.println("Logout successfully");
			Home.login();
			break;
		}
	}

}
