package com.eams.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;

import com.eams.bean.Admin;
import com.eams.bean.Employee;
import com.eams.bean.Leave;
import com.eams.controller.EmployeeOperations;
import com.eams.controller.Home;
import com.eams.util.DBConnectionUtil;
/**
 * This is the EmployeeDAOImpl class where various Employee  methods are implemented.
 * @author IMVIZAG
 *
 */
public class EmployeeDAOImpl implements EmployeeDAO {
	/**
	 * This method is used to register a new employee.
	 */
	@Override
	public boolean insertEmployeeDAO(Employee employee) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		con = DBConnectionUtil.getCon();
		String sql = "insert into employee (emp_id,emp_name,password,confirm_password,phone_number,email,address,date_of_joining,Department_id) values (?,?,?,?,?,?,?,?,?)";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, employee.getEmp_id());
			ps.setString(2, employee.getEmp_name());
			ps.setString(3, employee.getPassword());
			ps.setString(4, employee.getConfirm_password());
			ps.setString(5, employee.getPhone_number());
			ps.setString(6, employee.getEmail());
			ps.setString(7, employee.getAddress());
			ps.setString(8, employee.getDate_of_joining());
			ps.setInt(9, employee.getDepartment_id());
			int rowsEffected = ps.executeUpdate();
			System.out.println(rowsEffected);
			if (rowsEffected >= 1) {
				result = true;
		}
		}
		catch(SQLException e) {	
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
		}
	/**
	 * This method is used to search an employee by using emp_id.
	 */
	@Override
	public List <Employee> findByIdDAO(int id) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		con = DBConnectionUtil.getCon();
		List<Employee> empl = new ArrayList<Employee>();
		Employee employee2 = null;
		String sql = "select * from employee where emp_id = ?";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
			employee2 = new Employee();
			employee2.setEmp_id(rs.getInt(1));
			employee2.setEmp_name(rs.getString(2));
			employee2.setPassword(rs.getString(3));
			employee2.setConfirm_password(rs.getString(4));
			employee2.setPhone_number(rs.getString(5));
			employee2.setEmail(rs.getString(6));
			employee2.setAddress(rs.getString(7));
			employee2.setDate_of_joining(rs.getString(8));
			employee2.setDepartment_id(rs.getInt(9));
			empl.add(employee2);
		}
		}
		catch(SQLException e) {	
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return empl;
		}
	/**
	 * This method is used to Display all employee details to an admin.
	 */
	@Override
	public List<Employee> fetchAllEmployeeDAO(){
	Connection con = null;
	PreparedStatement ps = null;
	boolean result = false;
	Employee employee1 = null;
	con = DBConnectionUtil.getCon();
	List <Employee> employeeList = new ArrayList<Employee>();
	String sql = "select * from employee";
	try {
		con = DBConnectionUtil.getCon();
		ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			employee1 = new Employee();
			employee1.setEmp_id(rs.getInt(1));
			employee1.setEmp_name(rs.getString(2));
			employee1.setPassword(rs.getString(3));
			employee1.setConfirm_password(rs.getString(4));
			employee1.setPhone_number(rs.getString(5));
			employee1.setEmail(rs.getString(6));
			employee1.setAddress(rs.getString(7));
			employee1.setDate_of_joining(rs.getString(8));
			employee1.setDepartment_id(rs.getInt(9));
			
			employeeList.add(employee1);
	}
	}
	catch(SQLException e) {	
		e.printStackTrace();
	}
	finally {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return employeeList;
	}
	/**
	 * This method is used to delete an employee by using emp_id.
	 */
	@SuppressWarnings("finally")
	@Override
	public boolean deleteEmployeeDAO(int id) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean res = false;
		con = DBConnectionUtil.getCon();
		String sql = "delete from employee where emp_id = ?";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			int resu = ps.executeUpdate();
			System.out.println("Employee Deleted Successfully");
			if (resu >= 1) {
				res = true;
		
		}
		}
		catch(SQLException e) {	
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		return res;
		}
}
	/**
	 * This method is used to update an employee details by using emp_id.
	 */
	@SuppressWarnings("finally")
	@Override
	public boolean updateEmployeeDAO (int emp_id, String phone_number) {
		
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		Employee update = null;
		//con = DBConnectionUtil.getCon();
		//List <Employee> updateList = new ArrayList<Employee>();
		String sql = "update employee set phone_number = ? where emp_id = ? ";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(2, emp_id);
			ps.setString(1, phone_number );
			int rowsEffected = ps.executeUpdate();
			System.out.println(rowsEffected);
			
			if (rowsEffected >= 1) {
				result = true;
		}
		}
		catch(SQLException e) {	
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
		
		}
	/**
	 * This method is used to validate an employee login credentials.
	 */
	@Override
	public boolean employeeLogin(Employee credentials1) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//Employee credentials=null;
		//List<Employee> emplist = new ArrayList<Employee>();
		String sql = "select * from employee  ";
		boolean result = false;
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);			
			rs = ps.executeQuery();
			while(rs.next()) {
				if (rs.getString(2).equals(credentials1.getEmp_name()) && rs.getString(3).equals(credentials1.getPassword())) {				
				}
			}
		}
		catch (SQLException e){			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;

		
	}		
	
	/**
	 * This method calls all the employee login credentials from the database.
	 * @param username
	 * @return
	 */
	public static List<Employee> employeeLogin1(String username) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Employee credentials=null;
		List<Employee> emplist = new ArrayList<Employee>();
		try {
			con = DBConnectionUtil.getCon();
			String sql = "select emp_id,emp_name,phone_number,email,address,date_of_joining from employee where emp_name = ? ";
			ps = con.prepareStatement(sql);

			ps.setString(1,username);
			rs = ps.executeQuery();
			while(rs.next()) {
			int id = rs.getInt(1);
			int presentDays = 0;
			List<Employee> list = new AttendanceDAOImpl().fetchAttendance(id);


			for(Employee emp1 : list) {

				presentDays = emp1.getPresent_days();

			}
				credentials = new Employee();
				credentials.setEmp_id(rs.getInt(1));
				credentials.setEmp_name(rs.getString(2));
				credentials.setPhone_number(rs.getString(3));
				credentials.setEmail(rs.getString(4));
				credentials.setAddress(rs.getString(5));
				credentials.setDate_of_joining(rs.getString(6));
				credentials.setPresent_days(presentDays);
				emplist.add(credentials);
				}
		}
		catch (SQLException e){			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return emplist;

	}

	/**
	 * This method calls the all the employee leaves to the admmin
	 */
	@Override
	public List<Leave> fetchallLeaves() {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		Leave employee1 = null;
		con = DBConnectionUtil.getCon();
		List <Leave> employeeList = new ArrayList<Leave>();
		String sql = "select * from causal_leaves";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				employee1 = new Leave();
				employee1.setEmp_id(rs.getInt(1));
				employee1.setDate_of_leave(rs.getString(2));
				employee1.setReason(rs.getString(3));
				employeeList.add(employee1);
		}
		}
		catch(SQLException e) {	
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return employeeList;
	}
}