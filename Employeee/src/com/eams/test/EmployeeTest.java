package com.eams.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.eams.bean.Employee;
import com.eams.service.EmployeeServiceImpl;

public class EmployeeTest {
	EmployeeServiceImpl service = null;

	@Before
	public void setUp() {
		service = new EmployeeServiceImpl();

	}

	@After
	public void tearDown() {

		service = null;
	}

	Employee employ = new Employee();

	/**
	 * insertEmployee unit testing for negative scenario means values are existed in
	 * database
	 */
	@Test
	public void insertEmployee() {
		
		employ.setEmp_id(4);
		Employee.setEmp_name("Mounika");
		Employee.setPassword("Mounika@123");
		employ.setConfirm_password("Mounika@123");
		employ.setPhone_number("7894561230");
		employ.setEmail("mounika@gmail.com");
		employ.setAddress("Hyd");
		employ.setDate_of_joining("2018-12-31");
		employ.setDepartment_id(1);
		assertFalse(service.insertEmployee(employ));
	}

	@Before
	/**
	 * employeeLogin unit testing for positive scenario means values are existed in
	 * database
	 */
	@Test
	public void employeeLoginTest() {
		
		Employee.setEmp_name("Mounika");
		Employee.setPassword("Mounika@123");
		assertFalse(service.employeeLogin(employ));
		System.out.println("login successfull");
	}

	/**
	 * employeeLogin unit testing for negative scenario means values are not existed
	 * in database
	 */
	@Test
	public void employeeLoginTestNegative() {
		
		Employee.setEmp_name("swapna");
		Employee.setPassword("swapna@123");
		assertFalse(service.employeeLogin(employ));
		System.out.println("login  not successfull");
	}

	/**
	 * updateEmployee() unit testing with employee id for positive scenario if id
	 * existed in the employee table it updates the phone number
	 */
	@Test
	public void updateEmployee() {

		assertFalse(service.updateEmployee(4, "9989772193"));

	}

	/**
	 * updateEmployee() unit testing with employee id for negative scenario
	 *
	 */
	@Test
	public void updateEmployeeForNegative() {

		assertTrue(service.updateEmployee(6, "9989772193"));
		System.out.println("id not exist");

	}

	/**
	 * deleteEmployee() unit testing with employee id for positive scenario if id
	 * existed int the employee table it deletes the record
	 */
	@Test
	public void deleteEmployee() {
		assertFalse(service.deleteEmployee(5));
		System.out.println("employee deleted");
	}

	/**
	 * deleteEmployee() unit testing with employee id for negative scenario
	 */
	@Test
	public void deleteEmployeeForNegative() {
		assertFalse(service.deleteEmployee(9));
		System.out.println("employee deleted");
	}

	/**
	 * findByIdPositive() unit testing with employee id for positive scenario
	 */

	@Test
	public void findByIdPositive() {

		@SuppressWarnings("rawtypes")
		List list = service.findById(2);

		assertEquals(1, list.size());
	}

	/**
	 * findByIdPositive() unit testing with employee id for negative scenario
	 */
	@Test
	public void findByIdNegative() {

		@SuppressWarnings("rawtypes")
		List list = service.findById(0);

		assertEquals(1, list.size());
	}

}
