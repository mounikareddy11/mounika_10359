package com.eams.service;


import com.eams.bean.Admin;
/**
 * This is implementation class for the AuthenticationServices interface.
 * @author Batch - G
 *
 */
public class AdminServieImp implements AdminServices {
	/**
	 * this method is used to insert a new admin into database;
	 * @param adminDetails
	 * @return
	 */
	@Override
	public boolean insertAdmin(Admin adminDetails) {

		return false;
	}
	/**
	 *  this method is used to search an admin from the database.
	 * @param username
	 * @return
	 */
	@Override
	public Admin findByUsername(String username) {
		com.eams.dao.AdminDAOImpl obj=new com.eams.dao.AdminDAOImpl();
		return obj.searchByUsername(username) ;
		
	}
	/**
	 * this method is used to delete an admin from the database.
	 * @param username
	 * @return
	 */
	@Override
	public boolean deleteAdmin(String username) {
		// TODO Auto-generated method stub
		return false;
	}


	

}
