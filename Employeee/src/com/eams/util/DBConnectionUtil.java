package com.eams.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This class is used to connect Mysql database to our project through JDBC.
 * @author Batch - G
 *
 */
public class DBConnectionUtil {
	/**
	 * This method is used to establishing the connection.
	 * @return
	 */
	public static Connection getCon() {
		Connection con = null;
		
		try {
			//1.loading the driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			//2.establish the connection
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/eams", "root", "innominds");
						
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return con;
	}
	
	
	
}
