package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Display
 */
@WebServlet("/Display")
public class Display extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Display() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String username = request.getParameter("username");
		String email = request.getParameter("email");
		// creating printwriter object
		PrintWriter pw = response.getWriter();
		pw.println("Username:" + username);
		pw.println("Email: " + email);
		@SuppressWarnings("unused")
		// creating session
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		List<String> mybooks = (List<String>) request.getAttribute("mybooks");
		Iterator<String> i = mybooks.iterator();
		while (i.hasNext()) {
			String book = i.next();
			pw.println(book + "<br>");
		}

		// closing printwriter object
		pw.close();

	}

}
