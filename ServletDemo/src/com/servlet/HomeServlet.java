package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#dopost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//getting the parameters
		String firstName = request.getParameter("firstname");
		String email = request.getParameter("email");
		//creating session
		HttpSession session = request.getSession();
		//setting attributes to session
		session.setAttribute("myemail", email);
		//destroying the session
		session.setMaxInactiveInterval(10);
		String sid = session.getId();
		//creating printwriter object
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		out.println("Hello :"+firstName+"<br>");
		out.println("session id: "+sid+"<br>");
		out.println("<a href='TargetServlet'>Click here to See your email</a>");
		out.println("</body></html>");
		//closing printwriter object
		out.close();
		
	}

}
