package com.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Source
 */
@WebServlet("/Source")
public class Source extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Source() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	// String username = request.getParameter("username");
	 String pwd1 = request.getParameter("pwd1");
	 String pwd2 = request.getParameter("pwd2");
	// String email = request.getParameter("email");
	

		List<String> books = new ArrayList<String> ();
		books.add("java");
		books.add(".net");
		books.add("c/c++");
		books.add("QE");
		 RequestDispatcher rd=request.getRequestDispatcher("/Display");
	 if(pwd1.equals(pwd2)) {
		 request.setAttribute("mybooks", books);
		rd.forward(request, response);
		 
		 
	 }else {
		 response.sendRedirect("error.html");
	 }
	
	}

}
