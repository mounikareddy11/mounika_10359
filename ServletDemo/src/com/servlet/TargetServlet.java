package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class TargetServlet
 */
@WebServlet("/TargetServlet")
public class TargetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TargetServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// it uses the existed session 
		HttpSession session = request.getSession(false);
		//creating printwriter object
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		if(session!=null) {
			String sid = session.getId();
			String email = (String) session.getAttribute("myemail");
			out.println("session id: "+sid+"<br>");
			out.println("Email :"+email);
			session.invalidate();
		}
		else {
			out.println("Not Authorized to access this page");
		}
		out.println("</body></html>");
		// closing the out object
		out.close();
		
		
	}

}
