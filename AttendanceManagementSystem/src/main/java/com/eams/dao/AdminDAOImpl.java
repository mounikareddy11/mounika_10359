package com.eams.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.eams.entities.Admin;
import com.eams.util.HibernateUtil;

import sun.security.util.Password;


/**
 * This is implementation class for the AdminDAO interface.
 * @author Batch - G
 *
 */
public class AdminDAOImpl implements AdminDAO {
	
	
	/**
	 * This method is used to create an admin from the database.
	 * @return result
	 * 
	*/
	@Override
	public boolean createAdminDAO(Admin adminDetails) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.save(adminDetails);
		System.out.println("saved");
		txn.commit();
		session.close();
		return true;

	}
	
	
	/**
	 * this method is used to create an admin from the database.
	 * @return adminDetails
	 */	
	@Override
	public  Admin searchByUsername(String username) {
		Admin admin=new Admin();
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.save(username);
		System.out.println("saved");
		txn.commit();
		session.close();
		return admin;
	}
	
	
	/**
	 * this method is used to delete an admin from the database.
	 * @return result
	 */
	@Override
	public boolean deleteAdminDAO(String username) {

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		Admin Admin = session.get(Admin.class,username );
		session.delete(Admin);
		System.out.println("Deleted");
		txn.commit();
		session.close();
		return true;

	}
	
	
	/**
	 * This method is for adminlogin with his username and {@link Password
	 * @return password
	 * 
	 */
	@Override
	public String AdminLogin(String username, String password ) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		Admin Admin = session.get(Admin.class,username );
		session.save(username,password);
		System.out.println("Login");
		txn.commit();
		session.close();
		return "";
	}

	}
	
	

	
	
