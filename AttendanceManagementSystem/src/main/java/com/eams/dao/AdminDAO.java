package com.eams.dao;

import com.eams.entities.Admin;

/**
 * This is the AdminDAO interface where various Admin Abstract methods are created.
 * @author Batch - G
 *
 */
public interface AdminDAO {
	/**
	 * this method is used to insert a new admin into database.
	 * @param adminDetails
	 * @return
	 */
	boolean createAdminDAO( Admin adminDetails );
	/**
	 *  this method is used to search an admin from the database.
	 * @param username
	 * @return
	 */
	Admin searchByUsername(String username);
	/**
	 * this method is used to delete an admin from the database.
	 * @param username
	 * @return
	 */
	boolean deleteAdminDAO( String username );
    /**
     * this method is used to check the record is exist or not
     * @param username
     * @return
     */
	public String AdminLogin(String username, String password);
}
