package com.eams.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.eams.entities.Employee;
import com.eams.entities.Leave;
import com.eams.util.HibernateUtil;


/**
 * This is the EmployeeDAOImpl class where various Employee  methods are implemented.
 * @author BATCH-G
 *
 */
public class EmployeeDAOImpl implements EmployeeDAO {
	
	
	/**
	 * This method is used to register a new employee.
	 * @return result
	 */
	
	@Override
	public boolean insertEmployeeDAO(Employee employee) {
		

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.save(employee);
		System.out.println("saved");
		txn.commit();
		session.close();
		return true;

		}
		//return statement

	
//   This is for checking details are getter here or not
//	public static void main(String[] args) {
//		List<Employee> list = new EmployeeDAOImpl().findByIdDAO(1);
//		for(Employee emp:list) {
//			System.out.println(emp.getEmp_name());
//		}
//	}
	/**
	 * This method is used to search an employee by using emp_id.
	 * @return empl
	 * 
	 */
	
	@Override
	public List <Employee> findByIdDAO(int id) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.save(id);
		System.out.println("saved");
		txn.commit();
		session.close();
		return null;
		

		}
	/**
	 * This method is used to Display all employee details to an admin.
	 * @return employeeList
	 */
	
	@Override
	public List<Employee> fetchAllEmployeeDAO() {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		List<Employee> list = session.createQuery("FROM employee").list();
		Iterator<Employee> iterator = list.iterator();
		while (iterator.hasNext()) {
			Employee product = (Employee) iterator.next();
//			System.out.println("ProductID: " + product.getProductID() + "\nProductName: " + product.getProductName() + "\nProductPrice: "
//					+ product.getProductPrice() + "\nCategory: " + product.getCategory() +"\n -------------------------");

			System.out.println("displayed");
		}
		txn.commit();
		session.close();
		return list;
	}	
	
	
	
	/**
	 * This method is used to delete an employee by using emp_id.
	 * @return res
	 */	
	@SuppressWarnings("finally")
	@Override
	public boolean deleteEmployeeDAO(int id) {
		

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		Employee employee= session.get(Employee.class, id);
		session.delete( employee);
		System.out.println("Deleted");
		txn.commit();
		session.close();
		return true;
		}

	
	
	/**
	 * This method is used to update an employee details by using emp_id.
	 * @return true
	 */
	@SuppressWarnings("unused")
	@Override
	public boolean updateEmployeeDAO(int emp_id, String phone_number) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.update(phone_number);
		System.out.println("Updated");
		txn.commit();
		session.close();
		return false;
		}
	
	
	/**
	 * This method is used to validate an employee login credentials.
	 * @return password
	 */
	@Override
	public String employeeLogin(String username, String password) {

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.save(username,password);
		System.out.println("saved");
		txn.commit();
		session.close();
		return "";

		
	}
					
	
	/**
	 * This method calls all the employee login credentials from the database.
	 * @param username
	 * @return emplist
	 */
//	public List<Employee> employeeLogin1(String username) {
//		
//		Connection con = null;
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		Employee credentials=null;
//		//employee list is created
//		List<Employee> emplist = new ArrayList<Employee>();
//		
//		try {
//			//getting connection from database
//			con = DBConnectionUtil.getCon();
//			String sql = "select emp_id,emp_name,phone_number,email,address,date_of_joining,salary from employee where emp_name = ? ";
//			ps = con.prepareStatement(sql);
//
//			ps.setString(1,username);
//			rs = ps.executeQuery();
//			while(rs.next()) {
//			int id = rs.getInt(1);
//			int presentDays = 0;
//		    //employee list to fetch employeeAttendance
//			List<Employee> list = new AttendanceDAOImpl().fetchAttendance(id);
//
//
//			for(Employee emp1 : list) {
//
//				presentDays = emp1.getPresent_days();
//
//			}
//				credentials = new Employee();
//				credentials.setEmp_id(rs.getInt(1));
//				credentials.setEmp_name(rs.getString(2));
//				credentials.setPhone_number(rs.getString(3));
//				credentials.setEmail(rs.getString(4));
//				credentials.setAddress(rs.getString(5));
//				credentials.setDate_of_joining(rs.getString(6));
//				credentials.setSalary(rs.getDouble(7));
//				credentials.setPresent_days(presentDays);
//				//adding into the list
//				emplist.add(credentials);
//				}
//		}
//		catch (SQLException e){			
//		}
//		finally {
//			try {
//				//close connection
//				con.close();
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}
//		//return statement
//		return emplist;
//
//	}
//	

	/**
	 * This method calls the all the employee leaves to the admin
	 * @return employeeList
	 */
	@Override
	public List<Leave> fetchallLeaves(int emp_id) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		List<Employee> list = session.createQuery("FROM Leave").list();
		Iterator<Employee> iterator = list.iterator();
		while (iterator.hasNext()) {
//			Employee product = (Employee) iterator.next();
//			System.out.println("ProductID: " + product.getProductID() + "\nProductName: " + product.getProductName() + "\nProductPrice: "
//					+ product.getProductPrice() + "\nCategory: " + product.getCategory() +"\n -------------------------");
System.out.println("all leaves");
	}
		return null;
	
}
}