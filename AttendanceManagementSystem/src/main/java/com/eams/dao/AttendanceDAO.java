//package com.eams.dao;
//
//import com.eams.entities.Leave;
//import com.eams.entities.Employee;
//import java.util.*;
//
///**
// * This is the AttendanceDAO interface where various Attendance fetching Abstract methods are created.
// * @author Batch - G
// *
// */
//public interface AttendanceDAO {
//	
//	
//	/**
//	 * this method is used to insert attendance values into a database.
//	 * @param id
//	 * @return
//	 */
//	boolean Attendance(int id);
//	
//	
//	/**
//	 * This method is used displaying the employee attendance details to an Admin.
//	 * @return
//	 */
//	List<Employee> AttendanceDetails ();
//	
//	
//	/**
//	 * This method is used displaying the employee attendance details to an Employee.
//	 * @param id
//	 * @return
//	 */
//	List <Employee> fetchAttendance(int id);
//	
//	
//	/**
//	 * This method is used to apply for a casual leave by an employee
//	 * @param leave
//	 * @return
//	 */
//	boolean ApplyCausalleave(Leave leave);
//	
//	
//	/**
//	 * This method is used to apply for a Sick leave by an employee
//	 * @param leave
//	 * @return
//	 */
//	boolean ApplySickleave(Leave leave);
//}
