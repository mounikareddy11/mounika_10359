package com.eams.provider;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.eams.dao.AdminDAOImpl;
import com.eams.entities.Admin;
;

/**
 * This is the resource class which acts as web resource which is specified with
 * specific url
 * 
 * @author BATCH-G
 *
 */
@Path("/adminprovider")
public class AdminProvider {
	
	/**
	 * This is resource method which performs findByUsername operation
	 * @param username
	 * @return user
	 */
	@GET
	@Path("/findByUsername")
	@Produces("application/json")
	public String findByUsername(@QueryParam("username") String username) {
		
		//gson object created
		//Gson gson = new Gson();
		Admin  list = new AdminDAOImpl().searchByUsername(username);
		//String adminDetails = gson.toJson(list);
		return "adminDetails";
		
	}
	
	
	/**
	 * This is resource method which performs deleteAdmin operation
	 * @param username
	 * @return deleteData
	 */
	@DELETE
	@Path("/deleteadmin")
	@Produces("application/json")
	 public String deleteAdmin(@QueryParam("username") String username) {
		
		//gson object created
		// Gson gson = new Gson();
		 boolean delete =new AdminDAOImpl().deleteAdminDAO(username);
		// String deleteData =gson.toJson(delete);	 
		return "deleteData";

	 }
	
	
	/**
	 * This is resource method which performs insertAdmin operation
	 * @param String json
	 * @return json
	 */
	@SuppressWarnings("unused")
	@POST
	@Path("/insertAdmin")
	@Produces("application/json")
	@Consumes("application/json")
	public String insertAdmin(String json) {
		
		//gson object created
		//Gson gson = new Gson();
		//Admin admin = gson.fromJson(json, Admin.class);
		//boolean result = new AdminDAOImpl().createAdminDAO(admin);
		return json;	
			
    }

}
