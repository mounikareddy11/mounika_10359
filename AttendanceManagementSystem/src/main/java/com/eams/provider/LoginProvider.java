package com.eams.provider;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.eams.service.AdminServiceImpl;
import com.eams.service.EmployeeServiceImpl;

/**
 * This is the resource class which acts as web resource which is specified with
 * specific url
 * 
 * @author BATCH-G
 *
 */
@Path("/login")
public class LoginProvider {
	/**
	 * This is resource method which performs Adminlogin operation
	 * 
	 * @param username
	 * @param password
	 * @return res
	 */
	@POST
	@Path("/admin")
	@Produces("application/json")
	public String Adminlogin(@QueryParam("username") String Admin_username, @QueryParam("password") String Admin_password) {
		// calling service method
		boolean isLogin = new AdminServiceImpl().AdminLogin(Admin_username, Admin_password);
		String res = isLogin ? Admin_username : "not user";
		return res;
	}
	
	/**
	 * This is resource method which performs Employeelogin operation
	 * 
	 * @param username
	 * @param password
	 * @return result
	 */
	@POST
	@Path("/employee")
	@Produces("application/json")
	public String EmployeeLogin(@QueryParam("username") String emp_name, @QueryParam("password") String password) {
		 boolean isLogin = new EmployeeServiceImpl().employeeLogin(emp_name, password);
		
		String result = isLogin ? emp_name : "not an user";
		return result;
				
   }
}
