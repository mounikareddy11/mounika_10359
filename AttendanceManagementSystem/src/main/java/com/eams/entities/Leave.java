package com.eams.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This Leave class is used to generate setters and getter methods for employee leaves.
 * @author Batch - G
 *
 */
@Entity
@Table(name="Leave")
public class Leave {
	@Id
	@Column(name="reason")
	private String reason;
	
	@Column(name="date_of_leave")
	private String date_of_leave;
	
	@Column(name="emp_id")
	private int emp_id;
	
	
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getDate_of_leave() {
		return date_of_leave;
	}
	public void setDate_of_leave(String date_of_leave) {
		this.date_of_leave = date_of_leave;
	}
	public int getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}
	
}
