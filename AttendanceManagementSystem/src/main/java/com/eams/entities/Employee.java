package com.eams.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This Employee class is used to generate setters and getter methods for
 * Employee credentials.
 * 
 * @author Batch - G
 *
 */
@Entity
@Table(name = "Employee")
public class Employee {
	@Id
	@Column(name = "emp_id")
	private int emp_id;

	@Column(name = "emp_name")
	private String emp_name;

	@Column(name = "password")
	private String password;

	@Column(name = "confirm_password")
	private String confirm_password;

	@Column(name = "phone_number")
	private String phone_number;

	@Column(name = "email")
	private String email;

	@Column(name = "address")
	private String address;

	@Column(name = "date_of_joining")
	private String date_of_joining;

	@Column(name = "Department_id")
	private int Department_id;

//	@Column(name = "salary")
//	private double salary;
//
//	@Column(name = "present_days")
//
//	private int present_days;

//	public double getSalary() {
//		return salary;
//	}
//
//	public void setSalary(double salary) {
//		this.salary = salary;
//	}
//
//	public String getAddress() {
//		return address;
//	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}

	public String getConfirm_password() {
		return confirm_password;
	}

	public String getEmp_name() {
		return emp_name;
	}

	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setConfirm_password(String confirm_password) {
//		{
//			if(Employee.password(Employee.getPassword(),confirm_password))
//			{
//				this.confirm_password = confirm_password;
//			}
//			else
//			{
//				System.out.println("password and confirm password doesn't matches....plz re-enter.....");
//			}
		this.confirm_password = confirm_password;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDate_of_joining() {
		return date_of_joining;
	}

	public void setDate_of_joining(String date_of_joining) {
		this.date_of_joining = date_of_joining;
	}

	public int getDepartment_id() {
		return Department_id;
	}

	public void setDepartment_id(int department_id) {
		Department_id = department_id;
	}

//	public int getPresent_days() {
//		return present_days;
//	}
//
//	public void setPresent_days(int present_days) {
//		this.present_days = present_days;
//	}

}
