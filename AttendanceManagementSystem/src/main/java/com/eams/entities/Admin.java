package com.eams.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This Admin class is used to generate setters and getter methods for admin credentials.
 * @author Batch - G
 *
 */
@Entity
@Table(name="Admin")
public class Admin {
	@Id
	@Column(name="Admin_id")
    private int Admin_id;
	@Column(name="Admin_username")
    private static String Admin_username;
	@Column(name="Admin_password")
    private static String Admin_password;
	@Column(name="Security_ques")
    private String Security_ques;
    
	public int getAdmin_id() {
		return Admin_id;
	}
	public void setAdmin_id(int admin_id) {
		Admin_id = admin_id;
	}
	public String getAdmin_username() {
		return Admin_username;
	}
	public static void setAdmin_username(String admin_username) {
		Admin_username = admin_username;
	}
	public static String getAdmin_password() {
		return Admin_password;
	}
	public static void setAdmin_password(String admin_password) {
		Admin_password = admin_password;
	}
	public String getSecurity_ques() {
		return Security_ques;
	}
	public void setSecurity_ques(String security_ques) {
		Security_ques = security_ques;
	}
}

