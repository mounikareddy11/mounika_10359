package com.eams.service;
import java.util.List;

import com.eams.entities.Employee;
import com.eams.entities.Leave;


/**
 * This is EmployeeService interface where all business logics are inserted. 
 * @author Batch - G
 *
 */
public interface EmployeeService {
	
	
	/**
	 * This method is used to register a new employee.
	 * @param employee
	 * @return
	 */
	boolean insertEmployee( Employee employee );
	
	
	/**
	 * This method is used to search an employee by using emp_id.
	 * @param id
	 * @return
	 */
	List<Employee> findById(int id);
	
	
	/**
	 * This method is used to Display all employee details to an admin.
	 * @return
	 */
	List<Employee> fetchAllEmployee();
	
	
	/**
	 * This method is used to delete an employee by using emp_id.
	 * @param id
	 * @return
	 */
	boolean deleteEmployee( int id );
	
	
	/**
	 * This method is used to update an employee details by using emp_id.
	 * @param emp_id
	 * @param phone_number
	 * @return
	 */
	boolean updateEmployee (int emp_id, String phone_number);
	
	
	/**
	 * This method is used to validate an employee login credentials.
	 * @param credentials1
	 * @return
	 */
	boolean employeeLogin(String emp_name, String password);
	
	
	/**
	 * This method calls the all employee leave data from the database.
	 * @return
	 */
	List<Leave> fetchAllLeave(int emp_id);
	
	
}
