package com.eams.service;

import com.eams.dao.AdminDAOImpl;
import com.eams.entities.Admin;


/**
 * This is implementation class for the AuthenticationServices interface.
 * @author Batch - G
 *
 */
public class AdminServiceImpl implements AdminServices {
	
	
	/**
	 * this method is used to insert a new admin into database;
	 * @param adminDetails
	 * @return false
	 */
	AdminDAOImpl admin=new AdminDAOImpl ();

	/**
	 * this constructor is used to create the object
	

	/**
	 * this method calls the DAO package for creating the product
	 * @param product
	 * @return
	 */
	public boolean insertAdmin(Admin adminDetails) {

		return admin.createAdminDAO(adminDetails);
	}
//	@Override
//	public boolean insertAdmin(Admin adminDetails) {
//		
//		
//		AdminDAOImpl user = new AdminDAOImpl();
//		user.createAdminDAO(adminDetails);
//		return false;
//	}
//	
	
	/**
	 *  this method is used to search an admin from the database.
	 * @param username
	 * @return obj
	 */
	@Override
	public Admin findByUsername(String username) {
		
		
		AdminDAOImpl obj=new AdminDAOImpl();
	    return obj.searchByUsername(username);	
	}
	
	
	/**
	 * this method is used to delete an admin from the database.
	 * @param username
	 * @return result
	 */
	@Override
	public boolean deleteAdmin(String username) { 
		
		AdminDAOImpl deleteData = new AdminDAOImpl();
		boolean result=deleteData.deleteAdminDAO(username);
		return result;
	}
	
	
	/**
	 * this method is used to login admin from the database.
	 * @param username
	 * @return isuser
	 */
	@Override
	public boolean AdminLogin(String username, String password) {
        
		boolean isuser = false;
		String st= new AdminDAOImpl().AdminLogin(username, password);
		if (password != null && password.equals(st)) {
		isuser = true;
		}
		return isuser;	
		
	}

	

}


	



