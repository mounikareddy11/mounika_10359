package com.abc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//it represents the database table
@Entity
@Table
public class leaves {
	
	@Id
	@Column
	private int leaveId;
	@Column
	private String leaveType;

	 
	@ManyToOne(fetch=FetchType.LAZY)
	  @JoinColumn(name="OWNER_ID")
	  private Employee owner;
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public int getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}
	

}
