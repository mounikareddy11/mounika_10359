package com.abc.dao;

import com.abc.hibernate.entities.Account;

public interface AccountDao {
	 Account getAccountById(int accNo);
	 void insertAccount(Account account);
 void updateAccount(int accNo);
 void deleteAccount(int accNo);
}
