package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

@Repository
public class AccountDaoImpl implements AccountDao {

	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public Account getAccountById(int accNo) {

		Session session=sessionFactory.getCurrentSession();
		Account account=session.get(Account.class, accNo);
		return account;
	}
	@Override
	public void insertAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		//Account account = new Account();
		session.save(account);
		

	}
	@Override
	public void updateAccount(int accNo) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Account account = new Account();
		account.setAccName("accName");
		session.update(account);
		
	}
	@Override
	public void deleteAccount(int accNo) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Account account = new Account();
		account.setAccNo(accNo);
		session.delete(account);
		
	}
	
	

}
