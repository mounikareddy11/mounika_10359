package com.abc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.bean.UserDetails;

@Controller
public class AuthenticationController {

	@GetMapping("Registrationform")
	public String getRegistrationForm() {
		return "Register";
	}
	@GetMapping("Loginform")
	public String getLoginForm() {
		return "Login";
	}
	@PostMapping("register")
	public String doRegister(@ModelAttribute UserDetails user,ModelMap map) {

		if((user.getPassword().length() < 3)&&(user.getPhoneNo().length()<10)) {
		   return "registration_failure";	
		}
		else {
			
			//map.addAttribute("userDetails", user);
			return "registration_success";
			
		}
		
	}
	
	@PostMapping("login")
	public String doLogin(@RequestParam("Email") String Email, @RequestParam("password") String password,ModelMap map) {
		if (Email.equals("mounika@gmail.com") && (password.equals("1234"))) {
             map.addAttribute("myEmail",Email);
			return "Loginsuccess";

		} else {

			return "LoginFailure";

		}

	}

}
