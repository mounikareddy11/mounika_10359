package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@Controller
public class AccountController {
	@Autowired
	private AccountService accountService;
	
	@RequestMapping("/accounts/{id}")
	public String searchAccount(@PathVariable("id") int accNo,ModelMap map) {
		Account account=accountService.SearchAccountById(accNo);
		map.addAttribute("account", account);
		return "account_saved";
		
	}
//	@GetMapping("Accountform")
//	public String getAccountForm() {
//		return "AccountCreation";
//	}
	 @GetMapping("/save/{accNo}/{accName}")
		public String save(@PathVariable("accNo") int accNo,@PathVariable("accName") String accName,ModelMap map) {
		Account account=new Account();
		account.setAccNo(accNo);
		account.setAccName("accName");
		 map.addAttribute(account);
			 accountService.saveAccount(account);
			 return "insertion";
			
		}
//	 @GetMapping("/update/{accNo}")
//		public String update(@PathVariable("accNo") int accNo,ModelMap map) {
//		Account account=new Account();
//		account.setAccNo(accNo);
//		
//		 map.addAttribute(account);
//			 accountService.updateAccount(account);
//			 return "update";
//			
//		}
	 @GetMapping("/delete/{accNo}")
		public String delete(@PathVariable("accNo") int accNo,ModelMap map) {
		Account account=new Account();
		account.setAccNo(accNo);
		
		 map.addAttribute(account);
			 accountService.deleteAccount(accNo);
			 return "delete";
			
		}
	
	

}
