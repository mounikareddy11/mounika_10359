package com.abc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SampleController {
	@GetMapping("/hello")
	public String display() {
		return "display";
	}

	@GetMapping("/greets")
	public ModelAndView greetings() {

		String myData = "welcome to SpringMVc Programming";
		ModelAndView model = new ModelAndView("result", "data", myData);

		return model;
	}
}
