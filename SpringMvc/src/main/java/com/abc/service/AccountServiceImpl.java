package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDao;
import com.abc.hibernate.entities.Account;
@Service
@Transactional(readOnly = true)
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountDao accountDao;
	
	@Transactional
	@Override
	public Account SearchAccountById(int accNo) {
		// TODO Auto-generated method stub
		return accountDao.getAccountById(accNo);
	}
	@Transactional
	
	@Override
	public void saveAccount(Account account) {
		
		accountDao.insertAccount(account);
	}
	
	@Override
	public void deleteAccount(int accNo) {
		// TODO Auto-generated method stub
		accountDao.deleteAccount(accNo);
		
	}
	@Override
	public void updateAccount(int accNo) {
		// TODO Auto-generated method stub
		
		accountDao.updateAccount(accNo);
		
	}

}
