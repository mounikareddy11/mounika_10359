package com.abc.service;

import com.abc.hibernate.entities.Account;

public interface AccountService {

	 Account SearchAccountById(int accNo);
	 void saveAccount(Account account);
 void updateAccount(int accNo);
	 void deleteAccount(int accNo);
}
