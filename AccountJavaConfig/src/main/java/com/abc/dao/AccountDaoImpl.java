package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

@Repository
public class AccountDaoImpl implements AccountDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override

	public int createAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		session.save(account);
		return account.getAccno();
	}

	@Override
	public Account getAccountByAccNo(int accno) {

		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		return account;

	}

	@Override
	public void updateAccount(Account account) {
		
		Session session = sessionFactory.getCurrentSession();
		session.update(account);
	}

	@Override
	public void deleteAccount(int accno) {
		 Session session = sessionFactory.getCurrentSession();
	      Account account = session.byId(Account.class).load(accno);
	      session.delete(account);		

		
	}

}
