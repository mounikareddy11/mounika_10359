package com.abc.dao;

import com.abc.hibernate.entities.Account;

public interface AccountDao {

	int createAccount(Account account);
	Account getAccountByAccNo(int accno);
	void updateAccount(Account account);
	void deleteAccount(int accno);

	
}
