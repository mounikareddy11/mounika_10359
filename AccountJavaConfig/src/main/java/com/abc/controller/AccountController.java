
package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@RestController
public class AccountController {
	@Autowired	
AccountService accountService;

@PostMapping("/accountcreate")

public ResponseEntity<?>  createAccount(@RequestBody Account account) {
	
int id=	accountService.insertAccount(account);
	
	return  ResponseEntity.ok().body("New account is created with id:"+id);

	
}

	@GetMapping("/account/{id}")
public ResponseEntity<Account> get(@PathVariable("id") int accNo) {
   Account account = accountService.getAccountByID(accNo);
   if (account == null) {
         
         return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
     }
     return new ResponseEntity<Account>(account, HttpStatus.OK);
}
	
	@PostMapping("/accountupdate/{id}")
	   public ResponseEntity<?> update(@PathVariable("id") int accno, @RequestBody Account account) {
		   Account currentAccount = accountService.getAccountByID(accno);
	         
	        if (currentAccount==null) {
	           return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
	        }
		//Account currentAccount=new Account();
	        currentAccount.setAccno(account.getAccno());
	        currentAccount.setAccname(account.getAccname());
	        currentAccount.setBalance(account.getBalance());
	      
	        return new ResponseEntity<Account>(currentAccount, HttpStatus.OK);
		}

	   @DeleteMapping("/account/{id}")
	   public ResponseEntity<?> delete(@PathVariable("id") int accno) {
		  
		   Account account = accountService.getAccountByID(accno);
	        if (account == null) {
	           
	            return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
	        }
	        accountService.delete(accno);
	        return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);
	     
	   }


	}



