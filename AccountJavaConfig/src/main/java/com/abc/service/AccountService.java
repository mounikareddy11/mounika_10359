package com.abc.service;

import com.abc.hibernate.entities.Account;

public interface AccountService {

	int insertAccount(Account account);
	Account getAccountByID(int accno); 
	void updateAccount(Account account);
	void delete(int accno);
}
