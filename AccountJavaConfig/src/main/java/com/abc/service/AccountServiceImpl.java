package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDao;
import com.abc.hibernate.entities.Account;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountDao accountDao;

	@Override
	@Transactional
	public int insertAccount(Account account) {

		return accountDao.createAccount(account);

	}

	@Override
	@Transactional
	public Account getAccountByID(int accno) {
		// TODO Auto-generated method stub
		return accountDao.getAccountByAccNo(accno);
	}

	@Override
	public void updateAccount(Account account) {
		// TODO Auto-generated method stub
		 accountDao.updateAccount(account);
		
	}

	@Override
	public void delete(int accno) {
		// TODO Auto-generated method stub
		 accountDao.deleteAccount(accno);
		
	}

}
