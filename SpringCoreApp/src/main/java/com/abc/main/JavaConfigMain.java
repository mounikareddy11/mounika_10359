package com.abc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.abc.bean.Message;
import com.abc.javaconfiguration.ApplicationConfiguration;

public class JavaConfigMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context = 
				new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
		Message message = (Message) context.getBean(Message.class);
		message.displayMessage();
	}

}
