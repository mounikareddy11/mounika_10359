package com.abc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.bean.Message;

public class MessageMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context = 
				new ClassPathXmlApplicationContext("classpath:com/abc/config/context.xml");
	    
		Message message1 = (Message) context.getBean("msg");	
		Message message2 = (Message) context.getBean("msg");	
		//message1.displayMessage();
		if(message1==message2) {
			System.out.println("both refer to same bean object");
		}
		else {
			System.out.println("Different bean object");
		}
		
	}

}
