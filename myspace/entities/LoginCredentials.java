package com.myspace.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This is the entity class in which we can create database tables and generate
 * setters and getters
 * 
 * @author IMVIZAG
 *
 */

@Entity
//it represents the database table
@Table(name = "LoginCredentials")
public class LoginCredentials {

	// @Id represents the primary key attribute
	@javax.persistence.Id
	// it represents the database column
	@Column
	private int Id;
	@Column
	private String userName;
	@Column
	private String password;

	// setters and getters
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
