package com.myspace.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myspace.dao.EmployeeDao;
import com.myspace.entities.Employee;
import com.myspace.entities.Employee_Details;

/**
 * This is the service package which calls all the methods from dao package
 * 
 * @author IMVIZAG
 *
 */
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	/**
	 * this method calls the DAO package for creating the Employees
	 */
	@Override
	public Employee save(Employee employee) {
		return employeeDao.save(employee);
	}

	/**
	 * this method calls the DAO package for finding all the employees based on
	 * their ID
	 */
	@Override
	public Employee findByUsername(int id) {

		Optional<Employee> emp = employeeDao.findById(id);
		return emp.get();
	}

	/**
	 * this method calls the DAO package for displaying the Employees
	 */
	@Override
	public List<Employee_Details> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
