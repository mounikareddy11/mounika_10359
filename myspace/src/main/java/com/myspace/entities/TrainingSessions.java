package com.myspace.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This is the entity class in which we can create database tables and generate
 * setters and getters
 * 
 * @author IMVIZAG
 *
 */

@Entity
//it represents the database table
@Table(name = "training_sessions_tbl")
public class TrainingSessions {

	// @Id represents the primary key attribute
	@Id
	// it represents the database column
	@Column(name = "domain_id")
	private int domainId;
	@Column
	private String title;
	@Column(name = "start_date")
	private Date start;
	@Column(name = "end_date")
	private Date end;

	// setters and getters
	public int getDomainId() {
		return domainId;
	}

	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

}
