package com.myspace.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.myspace.entities.EmployeeDetails;
import com.myspace.entities.EmployeeLeaves;
import com.myspace.entities.EmployeeSalary;
import com.myspace.entities.LoginCredentials;
import com.myspace.service.EmployeeService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/myspace")
public class EmployeeControllerImpl{

	@Autowired
	private EmployeeService employeeService;
	/**
	 * this method is used for the employee to login into the application based on
	 * username
	 * password
	 * @param employee
	 * @return
	 */
	
	@PostMapping("/login")
	public ResponseEntity<?> doAuthenticate(@RequestBody LoginCredentials credentials) {
		// calling service method
		int empId = employeeService.doLogin(credentials.getUsername(), credentials.getPassword());

		if (empId == -1) {
			// if username wrong it returns 202 status code along with message
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
					.body("{\"status\":\"username does not exist\",\"statuscode\":" + 202 + "}");
		} else if (empId == -2) {
			// if password is wrong it returns 201 status code along with message
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
					.body("{\"status\":\"wrong password\",\"statuscode\":" + 201 + "}");
		}
		// if authentication is success it will return empId along with 200 status code
		// and success message.
		return ResponseEntity.status(HttpStatus.OK)
				.body("{\"status\":\"login success\",\"statuscode\":" + 200 + ",\"empId\":" + empId + "}");
	}
	/**
	 * this method is used for the employee to view his profile based on
	 * employee_id
	 * @param employee
	 * @return
	 */


	@GetMapping("/viewprofile/{employee_id}")
	public ResponseEntity<EmployeeDetails> getById(@PathVariable("employee_id") int id) {
		
		EmployeeDetails employee = employeeService.findById(id);
		if (employee != null) {
			return new ResponseEntity<EmployeeDetails>(employee,HttpStatus.OK);
		}
		
		return new ResponseEntity<EmployeeDetails>(HttpStatus.NO_CONTENT);
	
	}
 
	/**
	 * this method is used for the employee to search his profile based on
	 * employee_id
	 * @param employee_id
	 * @return
	 */
	@GetMapping("/searchEmployee/{employee_id}")
	public @ResponseBody ResponseEntity<List<EmployeeDetails>> getAll(@PathVariable("employee_id") int id) {
		List<EmployeeDetails> entityList = employeeService.searchById(id);

        if(entityList!=null) {
		return new ResponseEntity<>(entityList, HttpStatus.OK); // XXX
        }
    	return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	/**
	 * this method is used for the employee to  update his profile  based on
	 * @param  employee_id
	 * @return
	 */
	@PutMapping("/updateprofile/{employee_id}")
	public ResponseEntity<EmployeeDetails> updateProfileById(@PathVariable("employee_id") int id,@RequestBody EmployeeDetails employee)
	{ 
		
		EmployeeDetails emp= employeeService.update(id,employee);
		if (emp == null) {
			return new ResponseEntity<EmployeeDetails>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<EmployeeDetails>(emp, HttpStatus.OK);
	}
	
	/**
	 * this method is used for the employee to apply for the leaves
	 * @param  employee_id
	 * @return
	 */
	@PostMapping("/leavedetails")
	public ResponseEntity<EmployeeLeaves> applyForLeave(@RequestBody EmployeeLeaves employee)
	{
		EmployeeLeaves leaves = employeeService.applyForLeave(employee);
		
		if (leaves == null) {
			return new ResponseEntity<EmployeeLeaves>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<EmployeeLeaves>(leaves, HttpStatus.OK);
	}
	
	/**
	 * this method is used for the employee to view his profile based on
	 * employee_id
	 * @param employee
	 * @return
	 */
	  
	@PutMapping("/salarydetails")
	public ResponseEntity<EmployeeSalary> salaryDetails(@RequestBody EmployeeSalary salarydetails)
	{
		EmployeeSalary details = employeeService.getSalaryDetails(salarydetails);
		
		
		if(details==null) {
			return new ResponseEntity<EmployeeSalary>(HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<EmployeeSalary>(details, HttpStatus.OK);
		}
}

    


