package com.myspace.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.myspace.dao.EmployeeDao;
import com.myspace.dao.EmployeeLeavesDao;
import com.myspace.dao.EmployeeSalaryDao;
import com.myspace.dao.LoginDao;
import com.myspace.entities.EmployeeDetails;
import com.myspace.entities.EmployeeLeaves;
import com.myspace.entities.EmployeeSalary;
import com.myspace.entities.LoginCredentials;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private LoginDao logindao;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private EmployeeLeavesDao leavesDao;

	@Autowired
	private EmployeeSalaryDao salaryDetailsDao;

	/**
	 * This service method calls dao methods to retrieve records and authenticates
	 * and returns value to controller respectively.
	 */
	public int doLogin(String username, String password) {
		LoginCredentials loginCredentials = logindao.findByUsername(username);

		if (loginCredentials == null) {
			// if user name wrong it returns -1
			return -1;
		}
		if (!loginCredentials.getPassword().equals(password)) {
			// if password wrong it returns -2
			return -2;
		} else {
			// if credentials are correct it returns empId
			return loginCredentials.getEmployeeId();
		}
	}

	/**
	 * This Method gets employee from database based on id @param(employeeId)
	 */
	@Override
	public EmployeeDetails findById(int employeeId) {
		Optional<EmployeeDetails> details = employeeDao.findById(employeeId);
		if (details == null) {
			return null;
		}
		return employeeDao.findById(employeeId).get();
	}

	/**
	 * This method fetches all the profile details based on id and returns the list
	 */
	@Override
	public List<EmployeeDetails> searchById(int id) {
		Optional<EmployeeDetails> details = employeeDao.findById(id);
		List<EmployeeDetails> emp = new ArrayList<EmployeeDetails>();
		emp.add(details.get());
		return emp;
	}

	/**
	 * This method takes employee object and id based on the employee object details
	 * this method modifies the employee details of particular id
	 */
	@Override
	public EmployeeDetails update(int employeeId, EmployeeDetails employee) {
		EmployeeDetails emp = employeeDao.findById(employeeId).get();
		if (employee.getAlternativeMobileNumber() != null) {
			emp.setAlternativeMobileNumber(employee.getAlternativeMobileNumber());
		}
		if (employee.getCompetency() != null) {
			emp.setCompetency(employee.getCompetency());
		}
		if (employee.getPermanentLocation() != null) {
			emp.setPermanentLocation(employee.getPermanentLocation());
		}
		if (employee.getPersonalEmailId() != null) {
			emp.setPersonalEmailId(employee.getPersonalEmailId());
		}
		if (employee.getPrimaryMobileNumber() != null) {
			emp.setPrimaryMobileNumber(employee.getPrimaryMobileNumber());
		}
		if (employee.getWorkLocation() != null) {
			emp.setWorkLocation(employee.getWorkLocation());
		}
		if (employee.getWorkPlacePhoneNumber() != null) {
			emp.setWorkPlacePhoneNumber(employee.getWorkPlacePhoneNumber());
		}
		employeeDao.save(emp);
		return employeeDao.findById(employeeId).get();
	}

	/**
	 * This method displays leave type and dates and reason
	 */
	@Override
	public EmployeeLeaves applyForLeave( EmployeeLeaves employee) {
		EmployeeLeaves leaves = leavesDao.findById(employee.getEmployeeId()).get();

		if (employee.getTodate() != null) {
			leaves.setTodate(employee.getTodate());
		}
		if (employee.getFromdate() != null) {
			leaves.setFromdate(employee.getFromdate());
		}
		if (employee.getLeaveType() != null) {
			leaves.setLeaveType(employee.getLeaveType());
		}
		if (employee.getUsername() != null) {
			leaves.setUsername(employee.getUsername());
		}

		if (employee.getReason() != null) {
			leaves.setReason(employee.getReason());
		}
	
		leavesDao.save(leaves);
		return leavesDao.findById(employee.getEmployeeId()).get();

	}

	/**
	 * This method gets number of leaves from user and based on number of leaves the
	 * salary is deducted from employee salary
	 */
	@Override
	public EmployeeSalary getSalaryDetails(EmployeeSalary employeesalary) {

		EmployeeSalary salarydetails = salaryDetailsDao.findById(employeesalary.getEmployeeId()).get();
		EmployeeLeaves empleaves = leavesDao.findById(employeesalary.getEmployeeId()).get();

		
		if (employeesalary.getNumberOfLeaves() != 0 || employeesalary.getNumberOfLeaves() == 0) {
			salarydetails.setNumberOfLeaves(Integer.parseInt(empleaves.getTodate().substring(1,2))-Integer.parseInt(empleaves.getFromdate().substring(1,2)));
		 }
         
		
	 if(empleaves.getLeaveType().equalsIgnoreCase("casual")) {
		salarydetails.setEmployeesalary(salarydetails.getEmployeesalary()-(salarydetails.getNumberOfLeaves()*500));
		 }
		salaryDetailsDao.save(salarydetails);
		return salaryDetailsDao.findById(employeesalary.getEmployeeId()).get();

	}
}
