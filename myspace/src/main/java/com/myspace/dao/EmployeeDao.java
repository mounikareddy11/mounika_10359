package com.myspace.dao;
import org.springframework.data.repository.CrudRepository;
import com.myspace.entities.EmployeeDetails;

/**
 * This is the dao class which extends the crud repository
 * 
 * @author IMVIZAG
 *
 */

public interface EmployeeDao extends CrudRepository<EmployeeDetails, Integer>{
	
	

}