package com.myspace.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.myspace.entities.CulturalEvents;

/**
 * This is the dao class which extends the crud repository
 * 
 * @author IMVIZAG
 *
 */

@Repository
public interface EventDao extends CrudRepository<CulturalEvents, Integer> {

}
