package com.abc.service;

import com.abc.beans.Employee;
import com.abc.dao.EmployeeDAO;

public class EmployeeService {
	
	private EmployeeDAO empDao;

	public EmployeeDAO getEmpDao() {
		return empDao;
	}

	public void setEmpDao(EmployeeDAO empDao) {
		this.empDao = empDao;
	}
	public Employee save() {
		Employee emp=empDao.createEmp();
		return emp;
	}

}
