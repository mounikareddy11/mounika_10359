package com.abc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.beans.Employee;
import com.abc.controller.EmployeeController;

public class EmployeeMain {
	public static void main(String[] args) {
		
			// TODO Auto-generated method stub

		 
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/abc/config/context.xml");	
		EmployeeController controller=(EmployeeController) context.getBean(EmployeeController.class); 
		
		Employee emp= controller.saveEmp();
		  
		  System.out.println("Emp saved:"+emp.getEmpId());
		  
		}
		
	}


