package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class for a DeleteStudent
 */
@WebServlet("/DeleteStudent")
public class DeleteStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteStudent() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		StudentService service = new StudentServiceImpl();

		boolean result = false;
		// converting studentId string to integer by using parseInt method
		int studentId = Integer.parseInt(request.getParameter("Id"));
		PrintWriter out = response.getWriter();
		// calling findById method
		Student student = service.findById(studentId);
		if (student != null) {
			result = service.deleteStudent(studentId);
			out.print("<html><body>");

			if (result) {
				out.print("<h2>Student Deleted</h2>");
			} else {
				out.print("<h2>Something wrong</h2>");
			}
		} else {
			out.print("<h3>Student ID not found</h3>");
		}

		out.print("</body></html>");
		// closing printwriter object
		out.close();
	}

}
