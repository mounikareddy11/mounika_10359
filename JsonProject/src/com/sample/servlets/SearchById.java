package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;
import com.sample.util.JsonConverter;

/**
 * Servlet implementation class SearchById
 */
@WebServlet("/SearchById")
public class SearchById extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SearchById() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	
			throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");

		// TODO Auto-generated method stub
		int id = Integer.parseInt(request.getParameter("id"));
        StudentDAO service = new StudentDAOImpl();
        Student studentList = service.searchById(id);
		//creating printwriter object
		PrintWriter pw=response.getWriter();
		//creating object for JsonConverter class
   		JsonConverter jsonConverter = new JsonConverter();
   	    //calling convertToJson method
   		String result = jsonConverter.convertToJsonObject(studentList);
   		pw.println(result);
   		//closing printwriter object
   		pw.close();
	}

}

