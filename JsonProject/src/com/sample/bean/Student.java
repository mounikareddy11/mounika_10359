package com.sample.bean;
/**
 * this class is used to generate setters and getters for estudent deatils
 * @author IMVIZAG
 *
 */
public class Student {

	private int id;
	private  String name;
	private int age;
	private Address address;

	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
	
}
