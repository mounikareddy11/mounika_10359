package com.sample.service;

import java.util.List;

import com.sample.bean.Student;
/**
 * this class contains the abstract methods
 * @author IMVIZAG
 *
 */
public interface StudentService {
	/***
	 * this insertStudent method is used to insert the data into a table
	 * @param student
	 * @return
	 */
	boolean insertStudent( Student student );
	/***
	 * this method is used to search student by using the student id
	 * @param id
	 * @return
	 */
	Student findById(int id);
	/***
	 * this method is used to dispaly all the student details
	 * @return
	 */
	
	List<Student> fetchAllStudents();
	
	/***
	 * this method is used to delete a student
	 * @param id
	 * @return
	 */
	boolean deleteStudent(int id);
	/***
	 * this method is used to update a student by useng student id
	 * it returns true if the student deleted successfully.
	 * @param student
	 * @return
	 */

	boolean updateStudent(Student student);
	
}
