package com.abc.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.abc.entity.Product;
import com.abc.util.HibernateUtil;

/**
 * This class contains create, update, delete, display methods of the product
 * @author IMVIZAG
 *
 */
public class ProductDAO {

	/**
	 * this method is used to save details of the product in the database
	 * @param product
	 * @return
	 */
	public boolean create(Product product) {

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.save(product);
		System.out.println("saved");
		txn.commit();
		session.close();
		return true;

	}

	/**
	 * this method is used to delete the record from the database
	 * @param productID
	 * @return
	 */
	public boolean delete(String productID) {

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		Product product = session.get(Product.class, productID);
		session.delete(product);
		System.out.println("Deleted");
		txn.commit();
		session.close();
		return true;
	}

	/**
	 * this method is used to update the record in the database
	 * @param product
	 * @return
	 */
	public boolean update(Product product) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.update(product);
		System.out.println("Updated");
		txn.commit();
		session.close();
		return false;
	}

	/**
	 * This method is used to display all the products
	 * @return
	 */
	public List<Product> display() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		List<Product> list = session.createQuery("FROM Product").list();
		Iterator<Product> iterator = list.iterator();
		while (iterator.hasNext()) {
			Product product = (Product) iterator.next();
			System.out.println("ProductID: " + product.getProductID() + "\nProductName: " + product.getProductName() + "\nProductPrice: "
					+ product.getProductPrice() + "\nCategory: " + product.getCategory() +"\n -------------------------");

		}
		txn.commit();
		session.close();
		return list;
	}
}
