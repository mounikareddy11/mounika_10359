package com.student_registration.controllers;

import java.util.*;

import com.student_registration.validations.StudentValidations;
/**
 * this class used for taking the required inputs for the student registration and updataion also 
 * @author GROUP-B
 *
 */
public class StudentInputTaking {

	Scanner scan = new Scanner(System.in);

	/**
	 * This method asking for Student First Name and Validating First Name
	 * 
	 * @return studentFirstName
	 */
	public String askingName(String ShowName) {
		String name;
		do {
			System.out.print("Student "+ShowName + ":");
			name = scan.nextLine();
		} while (!StudentValidations.stringValidation(name));
		return name;
	}

	/**
	 * This method asking for Student Date of Birth and Validating DOB
	 * 
	 * @return studentDOB
	 */
	public String askingDateOfBirth() {
		String studentDOB;
		do {
			System.out.print("Student Date Of Birth(dd-mm-yyyy):");
			studentDOB = scan.next();
		} while (!StudentValidations.dateOfBirthValidation(studentDOB));
		return studentDOB;
	}

	/**
	 * This method asking for Student Mobile Number and Validating Mobile Number
	 * 
	 * @return studentMobileNo
	 */
	public String askingMobileNumber() {
		String studentMobileNo;
		do {
			System.out.print("Student MobileNo(+(91/01)-xxxxxxxxxx):");
			studentMobileNo = scan.next();
		} while (!StudentValidations.mobileNumberValidation(studentMobileNo));
		return studentMobileNo;
	}

	/**
	 * This method asking for Student Email ID and Validating Email ID
	 * 
	 * @return studentEmail
	 */
	public String askingEmailID() {
		String studentEmail;
		do {
			System.out.print("Student Email:");
			studentEmail = scan.next();
		} while (!StudentValidations.emailValidation(studentEmail));
		return studentEmail;
	}

	/**
	 * This method asking for student Gender
	 * 
	 * @return gender
	 */
	public String askingGender() {

		 while (true) {
			int choice = -1;
			innerwhileLoop: while (true) {
				System.out.print("Select Gender \n 1) male\n 2)female  ");
				try {
					choice = scan.nextInt();
					break innerwhileLoop;
				} catch (Exception e) {
					System.out.println("Please Enter only Integers");
				}
			}

			switch (choice) {
			case 1:
				return "male";
			case 2:
				return "female";
			default:
				System.out.println("Please Choose Correct Option");
				break;
			}

		}
	}

	/**
	 * This method asking for student address and validating student address
	 * 
	 * @return studentAddress
	 */
	public String askingStudentAddress() {
		String studentAddress;
		do {
			System.out.println("Student Address:");
			scan.nextLine();
			studentAddress = scan.nextLine();

		} while (!StudentValidations.addressValidation(studentAddress));
		return studentAddress;
	}

	/**
	 * This method asking for student Qualification and validating the qualification
	 * 
	 * @return studentQualification
	 */
	public String askingStudentQualification() {
		String studentQualification;
		boolean qualify;
		do {
			System.out.println("Student Qualification:");
			studentQualification = scan.next();
			qualify = StudentValidations.qualificationValidation(studentQualification);
		} while (!qualify);
		return studentQualification;
	}

	/**
	 * This method asking for Student Caste and validating Caste and assigning the
	 * total fees according to the caste
	 * 
	 * @return studentCaste
	 */
	public String askingCaste() {
		System.out.print("Select your caste: \n1)OC\n2)BC\n3)ST\n4)SC\n");
		int cast = scan.nextInt();
		switch (cast) {
		case 1:
			return "OC";
		case 2:
			return "BC";
		case 3:
			return "ST";
		case 4:
			return "SC";
		default:
			System.out.println("Please Choose Correct Option");
			break;
		}
		return null;
	}

	/**
	 * This method asking for Student Course and validating Course
	 * 
	 * @return studentCourse
	 */
	public String askingStudentCourse() {
	   while(true) {
		System.out.print("Enter Student Course:");
		String studentCourse = scan.next().toUpperCase();
		  switch(studentCourse){
			case "CSE":
				return "CSE";
			case "ECE":
				return "ECE";
			case "EEE":
				return "EEE";
			case "MECH":
				return "MECH";
			case "CIVIL":
			    return "CIVIL";
			default:
				System.out.println("Please enter the correct course from CSE/ECE/MECH/EEE/CIVIL");
				break;
			}
	   }
	}

	public int askingJoiningYear() {
		String JoiningYear;
		do {
			System.out.print("Student Joining year:");
			JoiningYear = scan.next();
		} while (!StudentValidations.joiningYear(JoiningYear));
		return Integer.parseInt(JoiningYear);
	}

	/**
	 * This method asking for Student fees
	 * 
	 * @return studentCourse
	 */
	public double askingFeesPaid() {
		double feesPaid;
		System.out.print("Student Fees Paid:");
		feesPaid = scan.nextDouble();
		return feesPaid;
	}
}
