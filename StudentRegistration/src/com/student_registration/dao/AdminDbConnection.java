
package com.student_registration.dao;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.TreeMap;

import com.student_registration.models.AdminModel;
import com.student_registration.utilities.UtilityDao;

/**
 * this class is created for doing all the CURD operations into the files
 * 
 * @author Group B
 *
 */
public class AdminDbConnection {
	static TreeMap<Integer, AdminModel> adminList = new TreeMap<Integer, AdminModel>();
	public static final String ADMIN_FILE_PATH = "AdminInfoFile";
	UtilityDao  utilityDao = new UtilityDao();

	/**
	 * this method returns the addminlist reads from the file
	 * 
	 * @return treemap of all the admins
	 */
	public TreeMap<Integer, AdminModel> getAdminList() {

		ObjectInputStream inStreem = utilityDao.getInputStreem(ADMIN_FILE_PATH);
		if (inStreem == null) {
			setAdminList(adminList);
			inStreem = utilityDao.getInputStreem(ADMIN_FILE_PATH);
		}
		if (inStreem != null) {
			try {
				@SuppressWarnings("unchecked")
				TreeMap<Integer, AdminModel> adminList = (TreeMap<Integer, AdminModel>) inStreem.readObject();
				return adminList;
			} catch (ClassNotFoundException e) {
			} catch (IOException e) {
			}
		}

		try {
			inStreem.close();

		} catch (IOException e) {}

		return null;
	}

	/**
	 * this method reset the admin list with the latest data into the file
	 * 
	 * @param adminList
	 */
	public  void setAdminList(TreeMap<Integer, AdminModel> adminList) {
	
		ObjectOutputStream outStreem = utilityDao.getOutPutStreem(ADMIN_FILE_PATH);
		if (outStreem != null) {
			try {
				outStreem.writeObject(adminList);
			} catch (Exception e) {}
		}
		try {
			outStreem.close();
		} catch (IOException e) {}

	}



	
}
