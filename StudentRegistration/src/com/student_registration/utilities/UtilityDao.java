package com.student_registration.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.student_registration.models.UtilityClass;
/**
 * this class is having the common file handlings 
 * @author IMVIZAG
 *
 */
public class UtilityDao {
	
	public static final String UTILITY_FILE_PATH = "UtilityFile";
	/**
	 * 
	 * this method returns the outputStreem to the given path file
	 * 
	 * @return ObjectOutputStreem or null and caller should close the streem
	 */
	public  ObjectOutputStream getOutPutStreem(String filePath) {
		FileOutputStream fis = null;
		ObjectOutputStream outputStreem = null;
		try {
			fis = new FileOutputStream(new File(filePath));
			outputStreem = new ObjectOutputStream(fis);
			return outputStreem;

		} catch (FileNotFoundException e) {

			e.printStackTrace();
			return null;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
		}
	}
	
	
	/**
	 * this method returns the ObjectInputStreem for the given file path
	 * 
	 * @param filePath
	 * @return ObjectInputStreem ,caller shold close the streem
	 */
	public ObjectInputStream getInputStreem(String filePath) {
		FileInputStream fis = null;
		ObjectInputStream inputStreem = null;
		try {
			fis = new FileInputStream(new File(filePath));
			inputStreem = new ObjectInputStream(fis);
			return inputStreem;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;

		} catch (IOException e) {
			return null;
		} finally {
		}

	}

	/**
	 * this method is getting the UtilityClass Object from the file
	 * 
	 * @return returns Utility class Object
	 */
	public UtilityClass getUtilityObject() {
		ObjectInputStream inStreem = getInputStreem(UTILITY_FILE_PATH);
		if (inStreem == null) {
			ObjectOutputStream outStreem = getOutPutStreem(UTILITY_FILE_PATH);
			if (outStreem != null) {
				UtilityClass utility = new UtilityClass();
				utility.setLastStuddentId(10000);
				inStreem = getInputStreem(UTILITY_FILE_PATH);
				try {
					outStreem.writeObject(utility);
				} catch (IOException e) {
				} finally {
					try {
						outStreem.close();
					} catch (IOException e) {
					}
				}
			}
		}

		if (inStreem != null) {
			try {
				UtilityClass utilityObj = (UtilityClass) inStreem.readObject();
				return utilityObj;
			} catch (ClassNotFoundException e) {}
			catch (IOException e) {} finally {
				try {
					inStreem.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	/**
	 * this method updates the utilityObct in the file
	 * 
	 * @param Utility class Object object
	 */
	public  void updateUtilityObj(UtilityClass object) {
		ObjectOutputStream outStreem = getOutPutStreem(UTILITY_FILE_PATH);
		if (outStreem != null) {
			try {
				outStreem.writeObject(object);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					outStreem.close();
				} catch (IOException e) {}
			}
		}

	}

}
