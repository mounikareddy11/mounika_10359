package com.eams.services;


import com.eams.DAO.AdminDAOImpl;
import com.eams.bean.Admin;

/**
 * This is implementation class for the AuthenticationServices interface.
 * @author Batch - G
 *
 */
public class AdminServieImp implements AdminServices {
	/**
	 * this method is used to insert a new admin into database;
	 * @param adminDetails
	 * @return
	 */
	@Override
	public boolean insertAdmin(Admin adminDetails) {

		return false;
	}
	/**
	 *  this method is used to search an admin from the database.
	 * @param username
	 * @return
	 */
	@Override
	public Admin findByUsername(String username) {
		AdminDAOImpl obj=new AdminDAOImpl();
		return obj.searchByUsername(username) ;
		
	}
	/**
	 * this method is used to delete an admin from the database.
	 * @param username
	 * @return
	 */
	@Override
	public boolean deleteAdmin(String username) {
		// TODO Auto-generated method stub
		return false;
	}
	/**
	 * this method is used to login admin from the database.
	 * @param username
	 * @return 
	 */
	@Override
	public boolean AdminLogin(String username, String password) {
        
		boolean isuser = false;
		String st= new AdminDAOImpl().AdminLogin(username, password);
		if (password != null && password.equals(st)) {
		isuser = true;
		}
		return isuser;
		
		
		
		
	}
	
}


	


