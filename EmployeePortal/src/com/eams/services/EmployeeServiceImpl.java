package com.eams.services;

import java.sql.Date;
import java.util.List;

import com.eams.DAO.AdminDAOImpl;
import com.eams.DAO.EmployeeDAO;
import com.eams.DAO.EmployeeDAOImpl;
import com.eams.bean.Employee;
import com.eams.bean.Leave;
/**
 * this is the implementation class for EmployeeService interface.
 * @author Batch - G
 *
 */
public class EmployeeServiceImpl implements EmployeeService {
	/**
	 * This method is used to register a new employee.
	 * @param employee
	 * @return
	 */
	@Override
	public boolean insertEmployee(Employee employee) {
		EmployeeDAOImpl user = new EmployeeDAOImpl();
		user.insertEmployeeDAO(employee);
		return false;
	}
	/**
	 * This method is used to search an employee by using emp_id.
	 * @param id
	 * @return
	 */
	@Override
	public List <Employee> findById(int id) {
//		EmployeeDAOImpl finduser = new EmployeeDAOImpl();
		List <Employee> list = new EmployeeDAOImpl().findByIdDAO(id);
		return list;
	}
	/**
	 * This method is used to Display all employee details to an admin.
	 * @return
	 */
	@Override
	public List<Employee> fetchAllEmployee() {
		EmployeeDAOImpl userData = new EmployeeDAOImpl();
		userData.fetchAllEmployeeDAO();
		List <Employee> emp = new EmployeeDAOImpl().fetchAllEmployeeDAO();
		return emp;
	}
	/**
	 * This method is used to delete an employee by using emp_id.
	 * @param id
	 * @return
	 */
	@Override
	public boolean deleteEmployee(int id) {
		EmployeeDAOImpl deleteData = new EmployeeDAOImpl();
		deleteData.deleteEmployeeDAO(id);
		return false;
	}
	/**
	 * This method is used to update an employee details by using emp_id.
	 * @param emp_id
	 * @param phone_number
	 * @return
	 */
	@Override
	public boolean updateEmployee(int emp_id, String phone_number) {
		
		EmployeeDAOImpl updateData = new EmployeeDAOImpl();
		updateData.updateEmployeeDAO(emp_id ,phone_number);
		boolean update = new EmployeeDAOImpl().updateEmployeeDAO(emp_id, phone_number);
		return false;
	}
	/**
	 * This method is used to validate an employee login credentials.
	 * @param credentials1
	 * @return
	 */
	@Override
	public boolean employeeLogin(String emp_name, String password) {
		
		boolean isuser = false;
		String Str =new EmployeeDAOImpl().employeeLogin(emp_name, password);
		//EmployeeDAOImpl login = new EmployeeDAOImpl();
		//return login.employeeLogin(emp_name, password);
		if(password != null && password.equals(Str)) {
			isuser = true;
		}
		return isuser;
	
	}
	/**
	 * This method calls the all employee leave data from the database.
	 */
	@Override
	public List<Leave> fetchAllLeave() {
		List<Leave> leave = new EmployeeDAOImpl().fetchallLeaves();
		return leave;
	}

	
	
	


}
