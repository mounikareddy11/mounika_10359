package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class demospringbootapp {

	public static void main(String[] args) {
		SpringApplication.run(demospringbootapp.class, args);
	}

}

