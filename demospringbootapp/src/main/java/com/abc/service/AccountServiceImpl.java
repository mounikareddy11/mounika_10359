package com.abc.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountRepository;
import com.abc.entities.Account;
@Service
@Transactional
public class AccountServiceImpl  implements AccountService{

	@Autowired
	private AccountRepository accountRepository;
	@Override
	public Account save(Account account) {
		
		return accountRepository.save(account) ;
	}

	@Override
	public List<Account> findAll() {
		 List<Account> accountList = new ArrayList<Account>();
//		 repository.findAll().forEach(e -> accountList.add(e));
		 Iterable<Account> iAccount = accountRepository.findAll();
		 Iterator<Account> iterator = iAccount.iterator();
		
		 while(iterator.hasNext()) {
			 accountList.add(iterator.next());
		 }
		 return accountList;
	}

	@Override
	public Account findAccountById(int accno) {
		// TODO Auto-generated method stub
		Optional<Account> account =accountRepository.findById(accno);
		return  account.get();
	}

	@Override
	public void deleteAccount(int accno) {
		accountRepository.deleteById(accno);
		
	}

	@Override
	public Account updateAccount(Account account) {
		// TODO Auto-generated method stub
		return accountRepository.save(account);
	}

}
