package com.abc.dao;

import org.springframework.data.repository.CrudRepository;

import com.abc.entities.Account;

public interface AccountRepository  extends CrudRepository<Account,Integer>{

}
